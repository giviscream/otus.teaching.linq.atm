﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Models
{
    public class UserAccountHistory
    {
        public User user { get; set; }

        public IEnumerable<AccountHistory> accountsHistory { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Models
{
    public class AccountHistory
    {
        public Account account { get; set; }

        public IEnumerable<OperationsHistory> history { get; set; }
    }
}

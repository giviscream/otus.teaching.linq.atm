﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Models;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public User GetUserInfo(string userLogin, string userPassword)
        {
            return Users.Where(user => user.Login == userLogin && user.Password == userPassword).FirstOrDefault();
        }

        public IEnumerable<Account> GetUserAccounts(User user)
        {
            return Accounts.Where(account => account.UserId == user.Id);
        }

        public IEnumerable<AccountHistory> GetAccountsHistory(User user)
        {
            return Accounts.Where(userAcc => userAcc.UserId == user.Id)
                            .GroupJoin(History, acc => acc.Id, history => history.AccountId, (accRes, historyRes) => new AccountHistory (){account = accRes, history = historyRes });

        }

        public IEnumerable<UserAccountHistory> GetUsersInputs()
        {
            IEnumerable<AccountHistory> accountsHistory =
                    from history in History
                    join acc in Accounts
                    on history.AccountId equals acc.Id
                    where history.OperationType == OperationType.InputCash
                    group history by acc into accHistory
                    select new AccountHistory() { account = accHistory.Key, history = accHistory.AsEnumerable() };

            IEnumerable<UserAccountHistory> usersAccountsHistory =
                    from accHistory in accountsHistory
                    join users in Users
                    on accHistory.account.UserId equals users.Id
                    group accHistory by users into usersAccHistory
                    select new UserAccountHistory() { user = usersAccHistory.Key, accountsHistory = usersAccHistory.AsEnumerable() };

            return usersAccountsHistory;
        }

        public IEnumerable<User> GetUsersGreaterAmount(decimal sum)
        {
            return Users.Where(user => Accounts.Any(acc => acc.UserId == user.Id && acc.CashAll > sum));
        }
    }
}
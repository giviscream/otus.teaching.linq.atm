﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Models;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public static class ATMReport
    {

        private const string _notFound = "<NotFound>";

        public static void PrintUser(User user, int offset = 0)
        {
            if (user == null)
            {
                Console.WriteLine(_notFound);
            }
            else
            {
                Console.WriteLine(new string('\t', offset) + user);
            }
        }

        private static void PrintBankInfo<T>(IEnumerable<T> bankInfo, int offset = 0)
        {
            if (bankInfo == null || bankInfo.Count() == 0)
            {
                Console.WriteLine(_notFound);
            }
            else
            {
                Console.Write(new string('\t', offset));
                Console.WriteLine(string.Join("\n" + new string('\t', offset), bankInfo));
            }
        }

        public static void PrintUsers(IEnumerable<User> users, int offset = 0)
        {
            PrintBankInfo(users, offset);
        }

        public static void PrintAccounts(IEnumerable<Account> accounts, int offset = 0)
        {
            PrintBankInfo(accounts, offset);
        }

        public static void PrintHistory(IEnumerable<OperationsHistory> operationsHistory, int offset = 0)
        {
            PrintBankInfo(operationsHistory, offset);
        }

        public static void PrintAccountsHistory(IEnumerable<AccountHistory> accountsHistory, int offset = 0)
        {
            if (accountsHistory == null || accountsHistory.Count() == 0)
            {
                Console.WriteLine(_notFound);
            }
            else
            {
                foreach (AccountHistory accountHistory in accountsHistory)
                {
                    Console.WriteLine(new string('\t', offset) + accountHistory.account + ":");

                    PrintHistory(accountHistory.history, offset + 1);
                }
            }

        }

        public static void PrintUsersAccountHistory(IEnumerable<UserAccountHistory> usersAccountHistory, int offset = 0)
        {
            if (usersAccountHistory == null || usersAccountHistory.Count() == 0)
            {
                Console.WriteLine(_notFound);
            }
            else
            {
                foreach (UserAccountHistory userAccountHistory in usersAccountHistory)
                {
                    Console.WriteLine(new string('\t', offset) + userAccountHistory.user + ":");

                    PrintAccountsHistory(userAccountHistory.accountsHistory, offset + 1);
                }
            }
        }
    }
}

﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            string userLogin = "snow";
            string userPass = "111";
            Decimal accSum = 100400m;

            var atmManager = CreateATMManager();

            //__________________________________________________________Task1__________________________________________________________
            //Вывод информации о заданном аккаунте по логину и паролю;
            System.Console.WriteLine("User info:");

            var userInfo = atmManager.GetUserInfo(userLogin, userPass);
            ATMReport.PrintUser(userInfo, 1);

            //__________________________________________________________Task2__________________________________________________________
            //Вывод данных о всех счетах заданного пользователя;
            if (userInfo != null)
            {
                System.Console.WriteLine("Accounts info:");

                var accountsInfo = atmManager.GetUserAccounts(userInfo);
                ATMReport.PrintAccounts(accountsInfo, 1);

                //__________________________________________________________Task3__________________________________________________________
                //Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
                System.Console.WriteLine("Accounts History:");

                var accountsHistory = atmManager.GetAccountsHistory(userInfo);
                ATMReport.PrintAccountsHistory(accountsHistory, 1);
            }

            //__________________________________________________________Task4__________________________________________________________
            //Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;
            System.Console.WriteLine("Input Trans:");

            var usersInputs = atmManager.GetUsersInputs();
            ATMReport.PrintUsersAccountHistory(usersInputs, 1);

            //__________________________________________________________Task5__________________________________________________________
            //Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);
            System.Console.WriteLine($"People with sum > {accSum}:");

            var users = atmManager.GetUsersGreaterAmount(accSum);
            ATMReport.PrintUsers(users, 1);

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}